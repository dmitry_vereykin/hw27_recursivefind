/**
 * Created by Dmitry Vereykin on 7/25/2015.
 */
import java.util.Scanner;
import java.lang.Object;

public class RecursiveFindTom
{//Start of class
    public static void main(String[] args)
    {//Start of main
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the string  being searched: ");
        String string = in.nextLine();
        System.out.print("Enter the word we're looking for: ");
        String word = in.nextLine();

        boolean b = find(string, string, word);

        if (b)
        {
            System.out.println("The word \"" + word + "\" is found in that string.");
        }
        else
        {
            System.out.println("The word \"" + word + "\" is not found in that string.");
        }
    }//End of main

    public static boolean find(String str, String trunStr, String word)
    {
        if (str.equals(word) || trunStr.equals(word)){
            return true;
        }
        else if (str.length() == 0 || str.length() < word.length()){
            return false;


        }else{
            int debug1 = str.indexOf(word.charAt(0));
            int debug2 = word.length();
            int debug3 = str.length();
            if (str.length() >= word.length() && str.length() - str.indexOf(word.charAt(0)) >= word.length()){
                trunStr = str.substring(str.indexOf(word.charAt(0)), word.length() + str.indexOf(word.charAt(0)));
                str = str.substring(str.indexOf(word.charAt(0)) + 1);
            }else{
                return false;
            }

        }
        return find (str, trunStr, word); //<<<<<< DUMMY STATEMENT TO BE REPLACED.
    }//End of the find method.
}//End of class